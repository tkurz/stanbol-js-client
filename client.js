function Client(options) {

    var _url = options.url;

    if(_url == undefined) throw new Error("options.url must not be undefined");

    var obj = {

        enhancer : {

            options : {
                "in":"text",
                "out":"json",
                "enhancer":"default"
            },

            run : function(text,onsuccess,onerror,options) {

                var _options = jQuery.extend({},obj.enhancer.options,options);

                $.ajax({
                    url:_url+"/enhancer/chain/"+_options.enhancer+"?in="+_options.in+"&out="+_options.out,
                    type:"POST",
                    contentType:"text/plain",
		    data:text,
                    success:function(data) {
                        onsuccess(data);
                    },
                    error:function(obj) {
                        onerror(obj.statusText,obj.responseText);
                    }
                })

            }

        }

    }

    return obj;

}
