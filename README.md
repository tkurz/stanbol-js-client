JS Client Demo for Stanbol Webservice
================================

This is a simple JS client demo application, that enables a user to send textual to a stanbol endpoint and retrieve annotations
in various formats. The application is running on [http://demo2.newmedialab.at/index.html](http://demo2.newmedialab.at/index.html)

REST Webservice
===============

The Stanbol annotation webservice can easily be used via webservice. The service is available via POST request on

    http://demo2.newmedialab.at/stanbol/enhancer/chain/<NAME>?in=<IN>&out=<OUT>.

Supported values for path parameter *NAME*
-----------------------------------------
* **dbpedia** - accesses the dbpedia index
* **freebase** - accesses the freebase index

Supported values for query parameter *in*
-----------------------------------------
* **text** - content is of type text
* **html** - content is of type html
* **pdf** - content is of type pdf
* **office** - content is of type doc

Supported values for query parameter *out*
-----------------------------------------
* **json** - simple return format for annotations, serialized in json
* **xml** - simple return format for annotations, serialized in xml
* **jsonld** - annotation result in rdf, serialized in json
* **rdfxml** - annotation result in rdf, serialized in xml
* **turtle** - annotation result in rdf, serialized in turtle

A POST request, that sends a text file sample.txt to the analyzer can thus look like this:

    curl -X POST "http://demo2.newmedialab.at/stanbol/enhancer/chain/dbpedia?in=text&out=json" -T sample.txt

Simple return format
====================

Stanbol is able to return a simplified return format so the results can be used more easy in external applications.

Here is a suggestion for a JSON format:

    {
      'language' : '...',
      'sentiment' : {     (summarized sentiment value in case the sentiment engine is present)
        positive: ...,
        negative: ....
      },
      'annotations' : [
        {
          'label': '...', (preferred label of entity if available, or one of the labels occurring in the text otherwise)
          'type':  '...', (label of the type if available; otherwise skip; e.g. "person" or "location")
          'reference': '...', (URI of the linked entity if available; otherwise skip)
          'thumbnail': '...', (URI of a thumnail image that can be used to depict the linked entity if available)
          'summary': '...', (summary text of the referenced entity if available and query parameter "summary=true" is given)
          'positions' : [   (occurrences of this reference/label in the text)
            {
              start: ...,
              end: ...,
              confidence: ...,
              text: ...  (the label as it is occurring in the text)
            },
            ...
          ]
        }
      ],
      'categories' : [
        {
          'label': '...', (preferred label of the category)
          'reference': '...', (URI of the category if available)
          'summary' : ..., (if summary query parameter is given)
          'confidence': ...
      ]
    }
        
And the same for XML:

    <result>
      <language>...</language>
      <sentiment>
        <positive>...</positive>
        <negative>...</negative>
      </sentiment>
      <annotations>
        <annotation type="..." reference="...">
          <label>...</label>
          <thumbnail>...</thumbnail>
          <summary>...</summary>
          <positions>
            <position start="..." end="..." confidence="...">Text</position>
            ...
          </positions>
        </annotation>
        ...
      </annotations>
      <categories>
        <category reference="..." confidence="...">
          <label>...</label>
          <thumbnail>...</thumbnail>
          <summary>...</summary>
        </category>
        ...
      </categories>
    </result>


Notes
-----
* annotations are used for both, annotations that are already linked to a remote entity (EntityAnnotation) and annotations without a linked entity (TextAnnotation);
    * in case there exists an EntityAnnotation, the 'reference' field contains the URI of the entity, the label is the preferred label of the linked entity, and the type is the label of the type of the linked entity;
    * in case there is only a TextAnnotation, the 'reference' field is ommitted, the label is the label as it is occurring in the text, and the type is the type returned e.g. by the NER annotator
* summary and thumbnail are only inserted if the query parameters summary=true or thumbnail=true are given to the webservice request